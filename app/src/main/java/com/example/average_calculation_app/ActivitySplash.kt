package com.example.average_calculation_app

import android.content.Intent
import android.graphics.drawable.Animatable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*

class ActivitySplash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //animasyon

        var btnDownUp = AnimationUtils.loadAnimation(this, R.anim.down_up_button)
        btnAnime.animation = btnDownUp

        var balloonUpDown = AnimationUtils.loadAnimation(this, R.anim.up_down_balloon)

        ballonAnime.animation = balloonUpDown



        btnAnime.setOnClickListener {
            var btnDownUpClickButton=AnimationUtils.loadAnimation(this,R.anim.up_down_button)
            btnAnime.animation=btnDownUpClickButton

            var balloonUpDownClickBallon=AnimationUtils.loadAnimation(this,R.anim.down_up_balloon)
            ballonAnime.animation=balloonUpDownClickBallon

          btnAnime.startAnimation(btnDownUpClickButton)
            ballonAnime.startAnimation(balloonUpDownClickBallon)



           //CountDownTimer abstract sinif oldugu icin anonim class ile yapacagiz
           //kapanma animasyonunu goremiyoruz ozaman  CountDownTimer kullanmamiz lazım

            object : CountDownTimer(1000,1000){
                override fun onFinish() { //verdigin sn bittikten sonra ne yapmak istiyosun

                    //Intent(this,MainActivity::class.java) this kullanimiyoruz cunku anonim class icinden activity class erisme durumu var ayni class olsaydi this olurdu(activity erisebilirdi) ama ayni sinif icinde degil onun icin applicationContext kullanmamiz gerek

                    var intentA= Intent(applicationContext,MainActivity::class.java) //intent bir activity den baska bir activity gecerken kullanilir.
                    startActivity(intentA)
                }

                override fun onTick(millisUntilFinished: Long) { // countDown internal her aralik ne yapacak
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            }.start() // CountDownTimer mutlaka baslatmamiz lazım yoksa calismaz

            //Yan ekrana gecince veri kayboluyor
        }


    }
}
