package com.example.average_calculation_app

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.new_lesson_layout.*
import kotlinx.android.synthetic.main.new_lesson_layout.view.*
import kotlin.math.log


class MainActivity : AppCompatActivity() {

    private val lesson =
        arrayOf("Matematik", "Türkçe", "Fizik", "Edebiyat", "Algoritma", "Felsefe")
    private val allNotesInfo: ArrayList<MyLessons> =
        ArrayList(5) //Bu arraylist hem dersler hem notlar,hem krediler tututlacak

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //simple_dropdown_item_1line --> aslinda bir textview (androidin hazir yapisin kullandik kendimizde yapacagiz)
        var adapter =
            ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, lesson)
        autoTextViewLesson.setAdapter(adapter)

        //Lineer layaout icinde view(yani child) yoksa hesapla butonunu visiblite gizle
        buttonVisibleControl(rootLayout)

        btnLessonAdd.setOnClickListener {

            if (!autoTextViewLesson.text.isNullOrEmpty()) {

                var inflater =
                    LayoutInflater.from(this) // olay  tek main activity oalcagindan this diyoruz
                //yada boyle cagirabilir
                //var inflater3=getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater boylede cagirabilir.as LayoutInflater niye yaziyoruz burda casting islemi yapiyoruz bize donen Any gibi onu LayoutInflater tipine donusturuyoruz

                //ust child direk verme silme olusutruma islemlerinde sıkıntı yasanabilir null vermek herzaman daha mantıklı
                var tempView = inflater.inflate(
                    R.layout.new_lesson_layout,
                    null
                ) //hicbir seyin altina eklme ben elle kendm yaparim
                //temView icinde new_lesson_layout var müthis

                //scrollView.addView(tempView) scrollView tek child alir tempView buna ekleyemeyiz.

                tempView.autoNewTextViewLesson.setAdapter(adapter)

                tempView.btnLessonRemove.setOnClickListener {
                    rootLayout.removeView(tempView) //olusturulacak layoutta silme islemenide ekliyoruz ekledikten sonra silme  yapamayiz.
                    buttonVisibleControl(rootLayout)
                }


                //NOT: Ekledigimiz viewleri silme rootLayout eklemeden once belirtmeliyiz. Birden fazla layout olacagi icin hangisi erisecegimizi bilemeyiz.Layout silme islemini belirtmeliyiz.

                rootLayout.addView(tempView)
                btnCalculate.visibility = View.VISIBLE


                //statik verileri (xml dki) kullanicinin girdigi degerleri alalım

                var LessonName = autoTextViewLesson.text.toString()
                var lessonCredit = spnLessonKredi.selectedItem.toString()
                var lessonLetter = spnLessonNote.selectedItem.toString()

                //yeni ekledigimiz viewler text(parametre almiyor)
                tempView.autoNewTextViewLesson.setText(LessonName)
                tempView.spnNewLessonKredi.setSelection(
                    spinnerValueIndexFind(
                        spnLessonKredi,
                        lessonCredit
                    )
                )
                tempView.spnNewLessonNote.setSelection(
                    spinnerValueIndexFind(
                        spnLessonNote,
                        lessonLetter
                    )
                )

                reset()
            } else {

                FancyToast.makeText(
                    this, "Ders adini giriniz", FancyToast.LENGTH_LONG,
                    FancyToast.ERROR, true
                ).show()
            }

        }
    }

    fun averangeCalculate(view: View) { //view bize tiklandiginda verileri gelmesini saglayacak

        //TODO yapilacak

        var sumNote = 0.0
        var sumCredit = 0.0

        for (i in 0..rootLayout.childCount - 1) {
            var onlyLine = rootLayout.getChildAt(i) //i ogre child getiriyor

            var tempLesson = MyLessons(
                onlyLine.autoNewTextViewLesson.text.toString(),
                ((onlyLine.spnNewLessonKredi.selectedItemPosition) + 1).toString(), //1.kredi 2.kredi  bununlari indislerini aliyoruz
                (onlyLine.spnNewLessonNote.selectedItem).toString()
            )  //uygulama eklediklerimizi data class atacagiz
            allNotesInfo.add(tempLesson)

        }



        for (i in allNotesInfo) {

            sumNote += ((letterConvertToNote(i.lessonLetterNoteA))) * (i.lessonCreditA.toDouble())
            Log.e("HARF NOTU:" + i.lessonLetterNoteA, "lessonLetterNote")
            Log.e("HARF NOTU:" + i.lessonCreditA.toDouble(), "lessonCredit")
            sumCredit += i.lessonCreditA.toDouble()

        }


        var temp = (sumNote / sumCredit).toDouble()

        FancyToast.makeText(
            this, "Ortalama: " + temp, FancyToast.LENGTH_LONG,
            FancyToast.WARNING, true
        ).show()

        allNotesInfo.clear() //kullanici uygulamayi kapatmadan birkac kere not hesaplayabilir onceki verileri silmek gerekiyor


    }

    fun reset() {
        autoTextViewLesson.setText("")
        spnLessonKredi.setSelection(0)
        spnLessonNote.setSelection(0)
    }

    fun spinnerValueIndexFind(spinner: Spinner, findValue: String): Int {

        var index = 0
        for (i in 0..spinner.count) {

            if (spinner.getItemAtPosition(i).toString().equals(findValue)) {

                index = i
                break
            }
        }
        return index
    }

    fun buttonVisibleControl(view: View) {
        if (view.rootLayout.childCount == 0) btnCalculate.visibility =
            View.INVISIBLE else btnCalculate.visibility = View.VISIBLE
    }

    fun letterConvertToNote(temp: String): Double {
        var tempA: Double = 0.0
        when (temp) {

            "AA" -> tempA = 4.0
            "BA" -> tempA = 3.5
            "BB" -> tempA = 3.0
            "CB" -> tempA = 2.5
            "CC" -> tempA = 2.0
            "DC" -> tempA = 1.5
            "DD" -> tempA = 1.0
            "FF" -> tempA = 0.0
        }
        return tempA

    }
}
